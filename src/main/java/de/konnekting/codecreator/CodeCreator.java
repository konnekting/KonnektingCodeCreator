/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.konnekting.codecreator;

import de.konnekting.xml.konnektingdevice.v0.CommObject;
import de.konnekting.xml.konnektingdevice.v0.Device;
import de.konnekting.xml.konnektingdevice.v0.KonnektingDevice;
import de.konnekting.xml.konnektingdevice.v0.KonnektingDeviceXmlService;
import de.konnekting.xml.konnektingdevice.v0.Parameter;
import de.konnekting.xml.konnektingdevice.v0.ParameterBase;
import de.konnekting.xml.konnektingdevice.v0.ParameterGroup;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBException;
import org.xml.sax.SAXException;

/**
 *
 * @author achristian
 */
public class CodeCreator {

    private static void printHelp() {
        System.out.println("");
        System.out.println("KONNEKTING CodeCreator");
        System.out.println("-----------------------------");
        System.out.println("");
        System.out.println("Usage: ");
        System.out.println("    java -jar KONNEKTING-CodeCreator.jar inputfile.kdevice.xml");
        System.out.println("    java -jar KONNEKTING-CodeCreator.jar inputfile.kdevice.xml outputfile.h");
        System.out.println("");
    }

    private String sourceString = "";

    private final List<String> idnames = new ArrayList<>();
    private final List<String> comObjs = new ArrayList<>();
    private final List<String> params = new ArrayList<>();
    private final String coString;
    private final String paramString;
    private final KonnektingDevice konnektingDevice;

    public CodeCreator(File kdeviceFile) throws FileNotFoundException, JAXBException, SAXException, IOException {

        File template = new File("KONNEKTING_deviceconfiguration.h.template");
        File coTemplate = new File("COMOBJ.template");
        File paramTemplate = new File("PARAM.template");
        sourceString = getFileAsString(template);
        coString = getFileAsString(coTemplate);
        paramString = getFileAsString(paramTemplate);

        konnektingDevice = KonnektingDeviceXmlService.readConfiguration(kdeviceFile);

        Device device = konnektingDevice.getDevice();

        int manufacturerId = device.getManufacturerId();
        short deviceId = device.getDeviceId();
        short revision = device.getRevision();

        sourceString = replace(sourceString, "%MANUFACTURER_ID%", manufacturerId);
        sourceString = replace(sourceString, "%DEVICE_ID%", deviceId);
        sourceString = replace(sourceString, "%REVISION%", revision);

        for (CommObject co : device.getCommObjects().getCommObject()) {
            short id = co.getId();
            String idName = co.getIdName();
            String dpt = co.getDataPointType();
            byte flags = co.getFlags();
            String coLine = coString;

            coLine = replace(coLine, "%ID%", id);
            coLine = replace(coLine, "%IDNAME%", idName);
            coLine = replace(coLine, "%DPT%", dpt.replace(".", "_"));
            coLine = replace(coLine, "%FLAGS%", String.format("0x%02x", flags));
            comObjs.add(coLine);
            if (idName!=null) {
                idnames.add("#define COMOBJ_" + idName + " " + id);
            }
        }
        String comobjList = "";
        for (int i = 0; i < comObjs.size(); i++) {
            comobjList += comObjs.get(i);
            if (i < comObjs.size() - 1) {
                comobjList += ",\n";
            }
        }
        sourceString = replace(sourceString, "%COMOBJ_LIST%", comobjList);

        List<Parameter> pList = new ArrayList<>();

        for (ParameterGroup group : device.getParameters().getParameterGroup()) {
            for (ParameterBase p : group.getSpacerOrParameter()) {
                if (p instanceof Parameter) {
                    pList.add((Parameter)p);
                }
            }
        }

        pList.sort((Parameter p1, Parameter p2) -> {
            if (p1.getId() < p2.getId()) {
                return -1;
            } else if (p1.getId() > p2.getId()) {
                return +1;
            }
            return 0;
        });

        for (Parameter p : pList) {
            int id = p.getId();
            String idName = p.getIdName();
            String type = p.getValue().getType().name().toUpperCase().replaceAll("_", "");
            String paramLine = paramString;

            paramLine = replace(paramLine, "%ID%", id);
            paramLine = replace(paramLine, "%IDNAME%", idName);
            paramLine = replace(paramLine, "%TYPE%", type);
            params.add(paramLine);
            if (idName != null) {
                idnames.add("#define PARAM_" + idName + " " + id);
            }
        }
        String paramList = "";
        for (int i = 0; i < params.size(); i++) {
            paramList += params.get(i);
            if (i < params.size() - 1) {
                paramList += ",\n";
            }
        }
        sourceString = replace(sourceString, "%PARAM_LIST%", paramList);

        String idnameList = "";
        for (int i = 0; i < idnames.size(); i++) {
            idnameList += idnames.get(i);
            if (i < idnames.size() - 1) {
                idnameList += "\n";
            }
        }
        sourceString = replace(sourceString, "%IDNAMES%", idnameList);
    }

    private String getFileAsString(File template) throws IOException, FileNotFoundException {
        String fileContent = "";
        fileContent = new String(Files.readAllBytes(template.toPath()));
        return fileContent;
    }

    final String replace(String s, String placeholder, Object value) {
        s = s.replaceAll(placeholder, String.valueOf(value));
        return s;
    }

    public static void main(String[] args) throws JAXBException, SAXException, FileNotFoundException, IOException {

        if (args == null || args.length == 0) {
            printHelp();
        }
        if (args.length == 1) {
            File f = new File(args[0]);
            CodeCreator cc = new CodeCreator(f);
            cc.output();
        } else if (args.length == 2) {
            File f = new File(args[0]);
            File output = new File(args[1]);
            CodeCreator cc = new CodeCreator(f);
            cc.output(output);
        }

    }

    void output() throws IOException {
        Device device = konnektingDevice.getDevice();
        String deviceName = device.getDeviceName();
        String prefix = "kdevice_";
        String filename = "";

        if (deviceName == null || deviceName.isEmpty()) {
            filename = prefix + device.getManufacturerId() + "_" + device.getDeviceId() + "_" + device.getRevision();
        } else {
            filename = prefix + deviceName;
        }

        filename = filename.replaceAll("/", "-"); // convert / to -
        filename = filename.replaceAll("&", "+"); // convert & to +
        filename = filename.replaceAll("[^a-zA-Z0-9\\.\\-\\+]", "_"); // convert any other weird chat to _

        // Limit filename to 62+2 chars
        if (filename.length() > 62) {
            filename = filename.substring(0, 62);
        }
        filename += ".h";
        output(new File(filename));

    }

    void outputToFolder(File folder) throws IOException {
        System.out.println("Writing to folder: "+folder.getAbsolutePath());
        Device device = konnektingDevice.getDevice();
        String deviceName = device.getDeviceName();
        String prefix = "kdevice_";
        String filename = "";

        if (deviceName == null || deviceName.isEmpty()) {
            filename = prefix + device.getManufacturerId() + "_" + device.getDeviceId() + "_" + device.getRevision();
        } else {
            filename = prefix + deviceName;
        }

        filename = filename.replaceAll("/", "-"); // convert / to -
        filename = filename.replaceAll("&", "+"); // convert & to +
        filename = filename.replaceAll("[^a-zA-Z0-9\\.\\-\\+]", "_"); // convert any other weird chat to _

        // Limit filename to 62+2 chars
        if (filename.length() > 62) {
            filename = filename.substring(0, 62);
        }
        filename += ".h";
        System.out.println("Filename: "+filename);
        output(new File(folder, filename));

    }

    void output(File output) throws IOException {
        Files.write(output.toPath(), sourceString.getBytes(), StandardOpenOption.CREATE);
    }

}
